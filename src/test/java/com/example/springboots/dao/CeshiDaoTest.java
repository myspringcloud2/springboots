package com.example.springboots.dao;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.springboots.CeshiEntity;
@RunWith(SpringRunner.class)
@SpringBootTest
public class CeshiDaoTest {
	
	@Autowired
	CeshiDao ceshiDao;

	@Before
	public void setUp() throws Exception {
		System.out.println("ceshiDao==="+ceshiDao);
	}

	@Test
	public void testCount() {
		int i = ceshiDao.count();
		System.out.println("i = " + i);
	}

	@Test
	public void test() {
		List<Map<String, Object>> i = ceshiDao.queryList();
		System.out.println("i = " + i);
	}

	@Test
	public void testInsertCeshi() {
		CeshiEntity c = new CeshiEntity();
		c.setId("id10");
		 ceshiDao.insertCeshi(c );
		System.out.println("i = " );
	}

}
