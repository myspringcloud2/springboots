package com.example.springboots.dao;

import java.util.List;
import java.util.Map;

import com.example.springboots.CeshiEntity;

public interface CeshiDao {
    public int count();

    List<Map<String,Object>> queryList();

    void insertCeshi(CeshiEntity c);
}
