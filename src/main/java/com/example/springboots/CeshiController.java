package com.example.springboots;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@RestController
@EnableAutoConfiguration
public class CeshiController {
    @Resource
    private CeshiService ceshiService;

    @RequestMapping("/ceshi")
    public String ceshi(){
        return "ceshii";
    }

    @RequestMapping("/query")
    public int query(){
        return ceshiService.query();
    }

    @RequestMapping("/querylist")
    public List<Map<String,Object>> queryList(){
        return ceshiService.queryList();
    }
}
