package com.example.springboots;

import com.example.springboots.dao.CeshiDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

@Service
public class CeshiService{
    @Autowired
    private CeshiDao ceshiDao;

    public int query(){
        return ceshiDao.count();
    }

    public List<Map<String,Object>> queryList() {
        return ceshiDao.queryList();
    }
}
